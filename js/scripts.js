$( document ).ready(function() {
  AOS.init();
  $(window).on('load', function() {
    AOS.refresh();
  });
  var $menufooter = $('#menufooter');
  $menufooter.on('show.bs.collapse','.collapse', function() {
    $menufooter.find('.collapse.in').collapse('hide');
  });
  $(".menu").find("a").click(function(e) {
    e.preventDefault();
    var section = $(this).attr("href");
    $("html, body").animate({
        scrollTop: $(section).offset().top-40
    });
  });
  $(".icon-game").mouseover(function() {

    var className = jQuery(this).data('game');
    var nameMap = jQuery(this).data('gamefull');
    $('[data-gamefull = '+className+']').addClass('active');
    $('[data-gametext = '+className+']').addClass('active');
    $('.title-page').hide();
  });
  $(".icon-game").mouseout(function() { 
    var className = jQuery(this).data('game');
    var nameMap = jQuery(this).data('gamefull');
    $('[data-gamefull = '+className+']').removeClass('active');
    $('[data-gametext = '+className+']').removeClass('active');
    $('.title-page').show();
  });
});
$(window).scroll(function(){
  var sticky = $('.header'),
      scroll = $(window).scrollTop();

  if (scroll >= 100) sticky.addClass('fixed');
  else sticky.removeClass('fixed');
});
